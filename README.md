angularjs-authentifacation
==========================

An Angularjs module for managing authenfication with an API based Token

DEPENDENCIES
=============
https://github.com/grevory/angular-local-storage

configRouting is from:
https://github.com/fnakstad/angular-client-side-auth/tree/master/client/js

STARTER GUIDE
=============

include files in your project
<pre>
<script type="text/javascript" src="path/to/localstorage.js"></script>
<script type="text/javascript" src="path/to/angular-auth-manager.js"></script>
</pre>

<pre>
// Add auth-manager as a dependency to your app
angular.module('myApp', ['auth-manager']);

// To customize your config
angular.module('myApp').config(function(AuthManagerProvider) {});

// Add in controller
angular.module('myApp').controller('MainCtrl', function($scope, AuthManager) {});
</pre>

You need to configure an interceptor that add token in each request:

<pre>
var myApp = angular.module("myApp", []);
myApp.config(['$httpProvider', function($httpProvider) {
	 $httpProvider.interceptors.push('httpRequestAuthInterceptor');
}])
</pre>

RESPONSE ATTENDED BY THIS MODULE
========================================
Example for the token:

<pre>
	HTTP/1.1 200 OK
	x-accesstoken: {"accountID": "2ffd033b133b4637b398998c52a654a6", "secret": "90b01434b78c6260466ace1d1cd2518cb9aaeb76b5ba6354eb8f67cf7905ae58", "expires": "9999-12-31T23:59:59.999999Z"}
	content-type: application/json
	Cache-Control: no-cache
	Expires: Fri, 01 Jan 1990 00:00:00 GMT
	Content-Length: 248
	Server: Development/2.0
	Date: Mon, 30 Jun 2014 17:28:45 GMT
</pre>

Example for the account:

<pre>
	accountType: "user"
	avatar: "/img/avatars/avatar-blank.png"
	email: "joe@gmail.com"
	modified_time: "Mon, 30 Jun 2014 15:08:12 GMT"
	uid: "2ffd033b133b4637b398998c52a654a6"
	username: "joe@gmail.com"
</pre>


CONFIGURATION
=============
<pre>
var myApp = angular.module("myApp", []);
myApp.config(['AuthManagerProvider', function(AuthManagerProvider) {
	// put custom config here
}])
</pre>

#### roles
Default roles: anonymous, user, admin, superuser
*note: anonymous is always needed.
Take care that if you redefine new roles, you have to redefine all access levels.

<pre>
	AuthManagerProvider.setRoles(["role1", "role2", "role3"])
</pre>

#### access levels
default access level:

<pre>
accessLevels: {
	'public': "*",
	'anon': ['anonymous'],
	'user': ['user', 'admin', 'superuser'],
	'admin': ['admin', 'superuser'],
	'superuser': ['superuser']
}
</pre>

redefine access levels:
<pre>
var accessLevels = {
	'role1': "*",
	'role2': ['role2', 'role3'],
	'role3': ['role3']
}
AuthManagerProvider.setAccessLevels(
</pre>

#### api endpoint
there are a default API that you can use if you want for login/logout/register and refresh
default endpoint are:
<pre>
'REFRESH': '/api/v1/accounts/current/',
'LOGOUT': '/api/v1/accounts/logout/',
'LOGIN': '/api/v1/accounts/loginOrRegister/',
'REGISTER': '/api/v1/accounts/loginOrRegister/'
</pre>

You can redefine these endpoints by using:

<pre>
AuthManagerProvider.setApiEndpointLogout(newEndPoint);
AuthManagerProvider.setApiEndpointLogin(newEndPoint);
AuthManagerProvider.setApiEndpointRegister(newEndPoint);
AuthManagerProvider.setApiEndpointRefresh(newEndPoint);
</pre>

#### localstorage

default localStorage keys:
<pre>
'TOKEN': 'accessToken',
'ACCOUNT': 'account',
'LAST_REFRESH': 'lastRefresh'
</pre>

You can change it if you want:
<pre>
AuthManagerProvider.setLocalStorageKeyForToken('token');
AuthManagerProvider.setLocalStorageKeyForAccount('account');
AuthManagerProvider.setLocalStorageKeyForLastRefresh('lastrefresh');
</pre>

#### account model save in localStorage
default fields:
Important: these fields have to be send by your server in his response.

<pre>
var accountFields = ["uid", "email", "username", "accountType", "modified_time", "avatar"];
</pre>

you can redefine these fields:
<pre>
AuthManagerProvider.setAccountFields([...]);
</pre>

#### refresh time in milliseconds
check if we need to refresh the account after a certain amount of time.
default is 5 minutes

<pre>
AuthManagerProvider.setRefreshDataTimeoutMillis(10) // number represent minutes
</pre>

#### token key send by your API
default is: 'x-accesstoken'

<pre>
AuthManagerProvider.setHeadersTokenKey('token');
</pre>

TODO
====
Test and better example
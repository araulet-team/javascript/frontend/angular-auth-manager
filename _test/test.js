(function () {
    "use strict";

    describe("auth-manager module", function () {
        var authManagerProvider,
            authManagerFactory,
            ls,
            cache,
            $httpBackend;


//        beforeEach(module('auth-manager', function (AuthManagerProvider) {
//            authManagerProvider = AuthManagerProvider;
//        }));
//
//        beforeEach(function () {
//
//            inject(function (_$httpBackend_, _$cacheFactory_) {
//                $httpBackend = _$httpBackend_;
//                cache = _$cacheFactory_;
//
//            });
//
//        });

        describe('Default Configuration', function () {

            beforeEach(function () {
                inject(function (_AuthManager_) {
                    authManagerFactory = _AuthManager_;
                });
            });

            it('Should get the default value', function () {
                expect(authManagerFactory.account.get()).toBe('Default Value');
            });
        });

    });
})();

module.exports = function (grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        env: {
            dev: {
                src: "settings.json"
            }
        },

        uglify: {
            build: {
                files: {
                    'angular-auth-manger.min.js': ['angular-auth-manager.js']
                },
                nonull: true
            }
        },

        jshint: {
            options: {
                reporter: require('jshint-stylish')
            },
            beforeconcat: [
                'angular-auth-manager.js'
            ]
        },

        // grunt testem:run:basic
        testem: {

            basic: {
                // Options that will be passed to Testem
                options: {
                    test_page: "_test/layout-base.html",
                    parallel: 3,
                    launch_in_ci: ['Chrome', 'Firefox', 'Safari', 'IE7', 'IE8', 'IE9'],
                    launch_in_dev: ['Chrome', 'Firefox', 'Safari', 'PhantomJS']
                }
            }
        }
    });

    grunt.option("force", true);

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-contrib-testem');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('check', ['jshint']);
    grunt.registerTask('u', ['uglify']);
};
(function () {
    "use strict";

    var angularAuthManager = angular.module("AuthManager", []);

    angularAuthManager.provider("AuthManager", function AuthManagerProvider() {
        var config = {

            /* List all the roles you wish to use in the app
             * You have a max of 31 before the bit shift pushes the accompanying integer out of
             * the memory footprint for an integer
             */
            roles: [
                'anonymous',
                'user',
                'admin',
                'superuser'],

            /* Build out all the access levels you want referencing the roles listed above
             * You can use the "*" symbol to represent access to all roles
             */
            accessLevels: {
                'public': "*",
                'anon': ['anonymous'],
                'user': ['user', 'admin', 'superuser'],
                'admin': ['admin', 'superuser'],
                'superuser': ['superuser']
            }

        };

        /*
         Method to build a distinct bit mask for each role
         It starts off with "1" and shifts the bit to the left for each element in the
         roles array parameter
         */
        var buildRoles = function (roles) {

            var bitMask = "01";
            var userRoles = {};

            for (var role in roles) {
                var intCode = parseInt(bitMask, 2);
                userRoles[roles[role]] = {
                    bitMask: intCode,
                    title: roles[role]
                };
                bitMask = (intCode << 1 ).toString(2);
            }

            return userRoles;
        };

        var buildAccessLevels = function (accessLevelDeclarations, userRoles) {

            var accessLevels = {};
            for (var level in accessLevelDeclarations) {

                if (typeof accessLevelDeclarations[level] == 'string') {
                    if (accessLevelDeclarations[level] == '*') {

                        var resultBitMask = '';

                        for (var role in userRoles) {
                            resultBitMask += "1";
                        }
                        //accessLevels[level] = parseInt(resultBitMask, 2);
                        accessLevels[level] = {
                            bitMask: parseInt(resultBitMask, 2)
                        };
                    }
                    else console.log("Access Control Error: Could not parse '" + accessLevelDeclarations[level] + "' as access definition for level '" + level + "'");

                }
                else {

                    var resultBitMask = 0;
                    for (var role in accessLevelDeclarations[level]) {
                        if (userRoles.hasOwnProperty(accessLevelDeclarations[level][role])) {
                            resultBitMask = resultBitMask | userRoles[accessLevelDeclarations[level][role]].bitMask;
                        }
                        else  {
                            console.log("Access Control Error: Could not find role '" + accessLevelDeclarations[level][role] + "' in registered roles while building access for '" + level + "'");
                        }
                    }
                    accessLevels[level] = {
                        bitMask: resultBitMask
                    };
                }
            }

            return accessLevels;
        };

        var userRoles = (function () {
            return buildRoles(config.roles);
        })();

        var refreshDataTimeoutMillis = 5 * 60 * 1000;
        var headersTokenKey = "x-accesstoken";
        var localStorageKey = {
            'TOKEN': 'accessToken',
            'ACCOUNT': 'account',
            'LAST_REFRESH': 'lastRefresh'
        };
        var apiEndpoint = {
            'REFRESH': '/api/v1/accounts/current/',
            'LOGOUT': '/api/v1/accounts/logout/',
            'LOGIN': '/api/v1/accounts/loginOrRegister/',
            'REGISTER': '/api/v1/accounts/loginOrRegister/'
        };
        var accountFields = ["uid", "email", "username", "accountType", "modified_time", "avatar"];

        /* Account model store in memory/localStorage
         */
        var Account = function (params) {
            var i,
                tmp = {};

            for (i = 0; i < accountFields.length; i++) {
                var key = accountFields[i];
                tmp[key] = params[key] || 'undefined';
            }

            return tmp;
        };

        // setter for roles
        this.setRoles = function (roles) {
            if (!Array.isArray(roles)) {
                throw new Error("roles has to be an Array");
            }
            config.roles = roles;
        };

        // setter for accessLevels
        this.setAccessLevels = function (accessLevels) {
            config.accessLevels = accessLevels;
        };

        // setter for api endpoint
        this.setApiEndpointRefresh = function (endpoint) {
            apiEndpoint.REFRESH = endpoint;
        };

        this.setApiEndpointLogout = function (endpoint) {
            apiEndpoint.LOGOUT = endpoint;
        };

        this.setApiEndpointLogin = function (endpoint) {
            apiEndpoint.LOGIN = endpoint;
        };

        this.setApiEndpointRegister = function (endpoint) {
            apiEndpoint.REGISTER = endpoint;
        };

        // setter for account model
        this.setAccountFields = function (fields) {
            if (!Array.isArray(roles)) {
                throw new Error("account fields has to be an Array");
            }
            accountFields = fields;
        };

        // setter for accessLevels
        this.getAaccessLevels = function () {
            return buildAccessLevels(config.accessLevels, userRoles);
        };

        // setter for refreshDataTimeoutMillis
        this.setRefreshDataTimeoutMillis = function(minutes) {
            refreshDataTimeoutMillis = minutes * 60 * 1000;
        };

        // setter for token api key
        this.setHeadersTokenKey = function(key) {
            headersTokenKey = key;
        };

        this.$get = ['localStorageService', '$q', '$http', '$cacheFactory',
            function authManagerFactory(localStorageService, $q, $http, $cacheFactory) {

                var Auth = function () {
                    var deferred = $q.defer(),
                        auth = this;

                    init(this, function () {
                        deferred.resolve(auth);
                    });
                };

                Auth.prototype.save = function (data, headersFn) {
                    updateLastRefreshTime();
                    saveAccessTokenFromHeader(headersFn);
                    setAccount(this, data);
                };

                Auth.prototype.logout = function () {
                    /* remove token fron server
                     * clear all localStorage and cache in the application to re-start from a clean state
                     * init localStorage with an anonymous account (avoid error because role are encoded in
                     * router. So you need an anonymous account in localStorage)
                     */
                    logoutFromServer();
                    cleanAll();
                    setAccount(this);
                };

                Auth.prototype.account = function () {
                    return this._account;
                };

                Auth.prototype.authorize = function (accessLevel, bitMask) {

                    if (bitMask === undefined) {
                        bitMask = this._account.accountType;
                    }

                    return accessLevel.bitMask & bitMask;
                };

                Auth.prototype.isLoggedIn = function (user) {
                    if (user === undefined) {
                        user = this._account;
                    }

                    if (user.accountType === userRoles.anonymous.bitMask) {
                        return false;
                    }

                    // list all the bitMask considered as connected user
                    for (var idx in userRoles) {
                        if(user.accountType === userRoles[idx].bitMask) {
                            return true;
                        }
                    }

                    throw new Error("No userRoles were found for your accountType: ", user.accountType);
                };

                /* init user account when service is loading
                 *  - load account with localStorage data
                 *      - account can be anonymous if there are no data or a real user
                 *  - check if we need to refresh the data for a real user
                 *      we make this call after init because if we don't we'll have an
                 *      error for Auth.authorize() in config. The http call makes to
                 *      much time and the config file is parse before account initialization.
                 *      So by doing initFromLocalData() first we ensure to already have an account
                 *      initialize when Auth.authorize() is called. (even if data is outdated)
                 */
                var init = function (auth, callback) {
                    initFromLocalData(auth, callback);

                    if (needsRefresh()) {
                        refreshDataFromServer(auth, callback);
                    }
                };

                var needsRefresh = function () {
                    return needsRefreshBecauseDataIsStale();
                };

                var needsRefreshBecauseDataIsStale = function () {
                    var storedToken = getTokenFromLocalStorage(),
                        lastRefresh = localStorageService.get(localStorageKey.LAST_REFRESH),
                        now = null,
                        needsRefresh = !!storedToken;

                    if (lastRefresh && needsRefresh) {
                        now = new Date().getTime();
                        if (now - lastRefresh < refreshDataTimeoutMillis) {
                            needsRefresh = false;
                        }

                        if (needsRefresh) {
                            console.log("_needsRefreshBecauseDataIsStale:: now - lastRefresh: " + (now - lastRefresh));
                        }
                    }

                    return needsRefresh;
                };

                var updateLastRefreshTime = function () {
                    localStorageService.add(localStorageKey.LAST_REFRESH, new Date().getTime());
                };

                var initFromLocalData = function (auth, callback) {
                    var accountFields = localStorageService.get(localStorageKey.ACCOUNT);
                    setAccountInMemory(auth, accountFields);
                    callback();
                };

                var saveAccessTokenFromHeader = function (headersFn) {
                    var accessToken = parseAccessTokenFromHeaders(headersFn);

                    if (accessToken) { // might not be set if refreshing
                        setTokenInLocalStorage(accessToken);
                    } else {
                        console.log("No token was found - anonmous account");
                    }
                };

                var parseAccessTokenFromHeaders = function (headersFn) {
                    var headers = headersFn(),
                        token,
                        accessToken;

                    token = headers[headersTokenKey] ? headers[headersTokenKey] : null;

                    if (token) {
                        accessToken = JSON.parse(token);
                    } else {
                        accessToken = null;
                    }

                    console.log('parsed access token: ', token, '; from headers: ', headers);

                    return accessToken;
                };

                var setAccount = function (auth, data) {
                    setAccountInMemory(auth, data);
                    setAccountInLocalStorage(auth._account);
                };

                var cleanAll = function () {
                    localStorageService.clearAll();
                    $cacheFactory.removeAll();
                };

                /* API
                 */
                var buildResponseHeaders = function (obj) {
                    var tmp = [], prop;
                    for (prop in obj) {
                        tmp[prop] = obj[prop];
                    }
                    return tmp;
                };

                var refreshDataFromServer = function (auth, callback) {

                    $http({
                        method: "GET",
                        url: apiEndpoint.REFRESH,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                        .success(function (data, status, headersFn, config) {
                            auth.save(data, headersFn);
                            callback();
                        })
                        .error(function (reason, status) {
                            callback();
                        });
                };

                var logoutFromServer = function () {
                    var deferred = $q.defer();

                    $http({
                        method: "GET",
                        url: apiEndpoint.LOGOUT,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                        .success(function (data, status, headersFn, config) {
                            var response = buildResponseHeaders({data: data, status: status, headers: headersFn, config: config});
                            deferred.resolve(response);
                        })
                        .error(function (reason, status) {
                            deferred.reject(reason);
                        });

                    return deferred.promise;
                };

                var loginFromServer = function (data) {
                    var deferred = $q.defer();

                    $http({
                        method: "POST",
                        url: apiEndpoint.LOGIN,
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                        .success(function (data, status, headersFn, config) {
                            var response = buildResponseHeaders({data: data, status: status, headers: headersFn, config: config});
                            deferred.resolve(response);
                        })
                        .error(function (reason, status) {
                            deferred.reject(reason);
                        });

                    return deferred.promise;
                };

                var registerFromServer = function (data) {
                    var deferred = $q.defer();

                    $http({
                        method: "POST",
                        url: apiEndpoint.REGISTER,
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                        .success(function (data, status, headers, config) {
                            var response = buildResponseHeaders({data: data, status: status, headers: headers, config: config});
                            deferred.resolve(response);
                        })
                        .error(function (reason, status) {
                            deferred.reject(reason);
                        });

                    return deferred.promise;
                };

                /* -------------------------
                 *  ACCOUNT METHODS
                 * ------------------------- */
                var getAccountFromMemory = function () {
                    return this._account;
                };

                var setAccountInMemory = function (auth, fields) {
                    var formattedFields = setAccountType(fields);
                    auth._account = new Account(formattedFields);
                    console.log('======== auth._account in setAccountInMemory ========', auth._account);
                };

                var setAccountType = function (fields) {
                    var role = null;

                    if (!fields) {
                        fields = {};
                        role = userRoles.anonymous.bitMask;
                    }
                    else {

                        for (var idx in userRoles) {
                            // Why two check ?
                            // when we register or login server send to us a human readable role like role (first check)
                            // but when we refresh this value is now a bitMask (second check)
                            if(fields.accountType === userRoles[idx].title || fields.accountType === userRoles[idx].bitMask) {
                                role = userRoles[idx].bitMask;
                            }
                        }

                        // if role is null at this point there is problem
                        if(role === null) {
                            throw new Error("Role sent by your server was not found in config userRoles: ", fields.accountType);
                        }
                    }

                    fields.accountType = role;

                    return fields;
                };

                /* -------------------------
                 *  LOCAL STORAGE
                 * ------------------------- */
                var setAccountInLocalStorage = function (account) {
                    localStorageService.add(localStorageKey.ACCOUNT, account);
                };

                var getAccountFromLocalStorage = function () {
                    return localStorageService.get(localStorageKey.ACCOUNT);
                };

                var getTokenFromLocalStorage = function () {
                    return localStorageService.get(localStorageKey.TOKEN);
                };

                var setTokenInLocalStorage = function (token) {
                    localStorageService.set(localStorageKey.TOKEN, token);
                };

                return {
                    '_': new Auth(),
                    'api': {
                        current: refreshDataFromServer,
                        login: loginFromServer,
                        register: registerFromServer,
                        logout: logoutFromServer
                    },
                    'account': {
                        get: getAccountFromMemory,
                        set: setAccountInMemory
                    }
                };
            }];
    });

    // factory that add token to each request
    // you need to add it in config like this: $httpProvider.interceptors.push('httpRequestAuthInterceptor');
    angularAuthManager.factory('httpRequestAuthInterceptor',
        ['$q', 'localStorageService',
            function ($q, localStorageService) {
                return {
                    'request': function (config) {
                        config.headers = config.headers || {};

                        if (localStorageService.get('accessToken')) {
                            var token = localStorageService.get('accessToken');
                            console.log('authInterceptor - token', token.secret);
                            config.headers.Authorization = 'Token token="' + token.secret + '"';
                        }

                        return config || $q.when(config);
                    },

                    'requestError': function (rejection) {
                        return $q.reject(rejection);
                    }
                };
            }]);

}).call(this);
